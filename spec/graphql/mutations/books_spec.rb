# frozen_string_literal: true

require 'rails_helper'

module Mutations
  module Books
    RSpec.describe AddBook, type: :request do
      describe '.resolve' do
        it 'creates a book' do
          author = create(:author)
          expect do
            post '/graphql', params: { query: query(author_ids: [author.id.to_s]) }
          end.to change(Book, :count).by(1)
        end

        it 'returns a book' do
          author = create(:author)
          post '/graphql', params: { query: query(author_ids: [author.id.to_s]) }
          json = JSON.parse(response.body)
          data = json['data']['addBook']
          expect(data['book']).to include(
            'id' => be_present,
            'title' => 'Tripwire',
            'publishYear' => '1999-03-24',
            'genre' => 'Thriller'
          )
        end
      end

      def query(author_ids:)
        <<~GQL
          mutation {
            addBook(input: {
              params: {
                title: "Tripwire"
                publishYear: "1999-03-24"
                genre: "Thriller"
                authorIds: #{author_ids}
              }
            }
            ) {
              book {
                id
                title
                publishYear
                genre
              }
            }
          }
        GQL
      end
    end

    RSpec.describe UpdateBook, type: :request do
      describe 'resolve' do
        it 'updates a book' do
          author = create(:author)
          book = create(:book, authors: [author])

          post '/graphql', params: { query: query(book_id: book.id) }
          expect(book.reload).to have_attributes(
            'id' => be_present,
            'title' => 'Tripwire',
            'publish_year' => '1999-03-24'.to_date,
            'genre' => 'Thriller'
          )
        end

        it 'returns a book' do
          author = create(:author)
          book   = create(:book, authors: [author])
          post '/graphql', params: { query: query(book_id: book.id) }
          json = JSON.parse(response.body)
          data = json['data']['updateBook']
          expect(data['book']).to include(
            'id' => be_present,
            'title' => 'Tripwire',
            'publishYear' => '1999-03-24',
            'genre' => 'Thriller'
          )
        end
      end

      def query(book_id:)
        <<~GQL
          mutation {
            updateBook(input: {
              bookId: #{book_id}
              title: "Tripwire"
              publishYear: "1999-03-24"
              genre: "Thriller" }
            ) {
              book {
                id
                title
                publishYear
                genre
              }
            }
          }
        GQL
      end
    end

    RSpec.describe DeleteBook, type: :request do
      describe 'resolve' do
        it 'removes a book' do
          author = create(:author)
          book = create(:book, authors: [author])

          expect do
            post '/graphql', params: { query: query(book_id: book.id) }
          end.to change(Book, :count).by(-1)
        end

        it 'returns a book' do
          author = create(:author)
          book = create(:book, title: 'Hero', publish_year: '1999-03-24', genre: 'Horror', authors: [author])
          post '/graphql', params: { query: query(book_id: book.id) }
          json = JSON.parse(response.body)
          data = json['data']['deleteBook']
          expect(data['book']).to include(
            'id' => be_present,
            'title' => 'Hero',
            'publishYear' => '1999-03-24',
            'genre' => 'Horror'
          )
        end
      end

      def query(book_id:)
        <<~GQL
          mutation {
            deleteBook(input: {
              bookId: #{book_id} }
            ) {
              book {
                id
                title
                publishYear
                genre
              }
            }
          }
        GQL
      end
    end
  end
end
