# frozen_string_literal: true

require 'rails_helper'

module Mutations
  module Authors
    RSpec.describe AddAuthor, type: :request do
      describe '.resolve' do
        it 'creates a author' do
          expect do
            post '/graphql', params: { query: query }
          end.to change(Author, :count).by(1)
        end

        it 'returns a author' do
          post '/graphql', params: { query: query }
          json = JSON.parse(response.body)
          data = json['data']['addAuthor']
          expect(data['author']).to include(
            'id' => be_present,
            'firstName' => 'Tripwire',
            'dob' => '1993-09-10',
            'surName' => 'Thriller'
          )
        end
      end

      def query
        <<~GQL
          mutation {
            addAuthor(input: {
              params: {
                firstName: "Tripwire"
                dob: "1993-09-10"
                surName: "Thriller"
              }
            }
            ) {
              author {
                id
                firstName
                dob
                surName
              }
            }
          }
        GQL
      end
    end

    RSpec.describe UpdateAuthor, type: :request do
      describe 'resolve' do
        it 'updates a author' do
          author = create(:author)

          post '/graphql', params: { query: query(author_id: author.id) }
          expect(author.reload).to have_attributes(
           'id' => be_present,
           'first_name' => 'Tripwire',
           'dob' => '1993-09-10'.to_date,
           'sur_name' => 'Thriller'
         )
        end

        it 'returns a author' do
          author = create(:author)

          post '/graphql', params: { query: query(author_id: author.id) }
          json = JSON.parse(response.body)
          data = json['data']['updateAuthor']
          expect(data['author']).to include(
            'id' => be_present,
            'firstName' => 'Tripwire',
            'dob' => '1993-09-10',
            'surName' => 'Thriller'
          )
        end
      end

      def query(author_id:)
        <<~GQL
          mutation {
            updateAuthor(input: {
              authorId: #{author_id}
              firstName: "Tripwire"
              dob: "1993-09-10"
              surName: "Thriller" }
            ) {
              author {
                id
                firstName
                dob
                surName
              }
            }
          }
        GQL
      end
    end

    RSpec.describe DeleteAuthor, type: :request do
      describe 'resolve' do
        it 'removes a author' do
          author = create(:author)

          expect do
            post '/graphql', params: { query: query(author_id: author.id) }
          end.to change(Author, :count).by(-1)
        end

        it 'returns a author' do
          author = create(:author, first_name: 'Hero', dob: '1999-03-24', sur_name: 'Horror')
          post '/graphql', params: { query: query(author_id: author.id) }
          json = JSON.parse(response.body)
          data = json['data']['deleteAuthor']
          expect(data['author']).to include(
            'id' => be_present,
            'firstName' => 'Hero',
            'dob' => '1999-03-24',
            'surName' => 'Horror'
          )
        end
      end

      def query(author_id:)
        <<~GQL
          mutation {
            deleteAuthor(input: {
              authorId: #{author_id} }
            ) {
              author {
               id
                firstName
                dob
                surName
              }
            }
          }
        GQL
      end
    end
  end
end
