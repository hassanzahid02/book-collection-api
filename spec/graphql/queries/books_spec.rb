# frozen_string_literal: true

require 'rails_helper'

module Queries
  module Books
    RSpec.describe FetchBooks, type: :request do
      describe '.resolve' do
        it 'returns multiple books' do
          author = create(:author)
          books = create_list(:book, 10, authors: [author])
          first_book = books.first
          last_book = books.last
          post '/graphql', params: { query: query }
          json = JSON.parse(response.body)
          data = json['data']['fetchBooks']

          expect(data.count).to equal(10)

          expect(data.last).to include(
            'id' => first_book.id.to_s,
            'title' => first_book.title,
            'publishYear' => first_book.publish_year.to_s,
            'genre' => first_book.genre
          )

          expect(data.first).to include(
            'id' => last_book.id.to_s,
            'title' => last_book.title,
            'publishYear' => last_book.publish_year.to_s,
            'genre' => last_book.genre
          )
        end
      end

      def query
        <<~GQL
          query {
            fetchBooks{
             id
             title
             publishYear
             genre
            }
          }
        GQL
      end
    end

    RSpec.describe FetchBook, type: :request do
      describe 'resolve' do
        it 'returns a book' do
          author = create(:author)
          book = create(:book, authors: [author])
          post '/graphql', params: { query: query(book_id: book.id) }
          json = JSON.parse(response.body)
          data = json['data']['fetchBook']
          expect(data).to be_present
          expect(data).to include(
            'id' => book.id.to_s,
            'title' => book.title,
            'publishYear' => book.publish_year.to_s,
            'genre' => book.genre
          )
        end
      end

      def query(book_id:)
        <<~GQL
          query {
            fetchBook(
              id: #{book_id}
            ) {
              id
              title
              publishYear
              genre
            }
          }
        GQL
      end
    end

  end
end
