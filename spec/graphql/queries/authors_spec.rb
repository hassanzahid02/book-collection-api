# frozen_string_literal: true

require 'rails_helper'

module Queries
  module Authors
    RSpec.describe FetchAllAuthors, type: :request do
      describe '.resolve' do
        it 'returns multiple authors' do
          authors = create_list(:author, 10)
          first_author = authors.first
          last_author = authors.last
          post '/graphql', params: { query: query }
          json = JSON.parse(response.body)
          data = json['data']['fetchAllAuthors']

          expect(data.count).to equal(10)

          expect(data.last).to include(
            'id' => first_author.id.to_s,
            'firstName' => first_author.first_name,
            'surName' => first_author.sur_name,
            'dob' => first_author.dob.to_s
          )

          expect(data.first).to include(
            'id' => last_author.id.to_s,
            'firstName' => last_author.first_name,
            'surName' => last_author.sur_name,
            'dob' => last_author.dob.to_s
          )
        end
      end

      def query
        <<~GQL
          query {
            fetchAllAuthors{
              id
              firstName
              surName
              dob
            }
          }
        GQL
      end
    end

    RSpec.describe FetchAuthor, type: :request do
      describe 'resolve' do
        it 'returns a author' do
          author = create(:author)
          post '/graphql', params: { query: query(author_id: author.id) }
          json = JSON.parse(response.body)
          data = json['data']['fetchAuthor']
          expect(data).to be_present
          expect(data).to include(
            'id' => author.id.to_s,
            'firstName' => author.first_name,
            'surName' => author.sur_name,
            'dob' => author.dob.to_s
          )
        end
      end

      def query(author_id:)
        <<~GQL
          query {
            fetchAuthor(
              id: #{author_id}
            ) {
              id
              firstName
              dob
              surName
            }
          }
        GQL
      end
    end

    RSpec.describe FetchAuthors, type: :request do
      describe 'resolve' do
        it 'returns all authors except book author' do
          author = create(:author)
          create_list(:author, 10)
          book = create(:book, title: 'Hero', publish_year: '1999-03-24', genre: 'Horror', authors: [author])
          post '/graphql', params: { query: query(book_id: book.id) }
          json = JSON.parse(response.body)
          data = json['data']['fetchAuthors']
          expect(data.count).to equal(10)
          expect(data).not_to include(
            'id' => author.id.to_s,
            'firstName' => author.first_name,
            'surName' => author.sur_name,
            'dob' => author.dob.to_s
          )
        end
      end

      def query(book_id:)
        <<~GQL
          query {
            fetchAuthors(
              bookId: #{book_id}
            ) {
              id
              firstName
              dob
              surName
            }
          }
        GQL
      end
    end
  end
end
