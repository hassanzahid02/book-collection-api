
require 'rails_helper'

RSpec.describe Author, type: :model do
  it 'has a valid factory' do

    author = build(:author, first_name: nil)
    expect(author.valid?).to be_falsey

    author = build(:author, sur_name: nil)
    expect(author.valid?).to be_falsey

    author = build(:author, dob: nil)
    expect(author.valid?).to be_falsey

    author = build(:author, first_name: '')
    expect(author.valid?).to be_falsey

    author = build(:author, sur_name: '')
    expect(author.valid?).to be_falsey

    author = build(:author, dob: '')
    expect(author.valid?).to be_falsey

    author = build(:author, first_name: ' ')
    expect(author.valid?).to be_falsey

    author = build(:author, sur_name: ' ')
    expect(author.valid?).to be_falsey

    author = build(:author, dob: ' ')
    expect(author.valid?).to be_falsey

  end

  it 'must create author' do
    author = build(:author)
    expect(author.valid?).to be_truthy
    author.save!
    expect(author.present?).to be_truthy
  end

end
