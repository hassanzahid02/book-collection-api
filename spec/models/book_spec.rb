require 'rails_helper'

RSpec.describe Book, type: :model do
  it 'has a valid factory' do

    book = build(:book, title: nil)
    expect(book.valid?).to be_falsey

    book = build(:book, publish_year: nil)
    expect(book.valid?).to be_falsey

    book = build(:book, genre: nil)
    expect(book.valid?).to be_falsey

    book = build(:book, title: '')
    expect(book.valid?).to be_falsey

    book = build(:book, publish_year: '')
    expect(book.valid?).to be_falsey

    book = build(:book, genre: '')
    expect(book.valid?).to be_falsey

    book = build(:book, title: ' ')
    expect(book.valid?).to be_falsey

    book = build(:book, publish_year: ' ')
    expect(book.valid?).to be_falsey

    book = build(:book, genre: ' ')
    expect(book.valid?).to be_falsey

  end

  it 'must have author' do
    author = build(:author)
    book = build(:book, authors: [author])
    expect(book.valid?).to be_truthy
  end

  it 'must contain author' do
    author = build(:author)
    book = build(:book, authors: [author])
    book.save!
    expect(book.present?).to be_truthy
    expect(book.authors.exists?).to be_truthy
  end
end
