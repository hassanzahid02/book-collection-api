FactoryBot.define do
  factory :book do
    sequence(:title) { |n| "Best book ever (#{n})" }
    publish_year { 20.years.ago }
    genre { 'Horror' }
  end
end