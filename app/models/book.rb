class Book < ApplicationRecord

  has_and_belongs_to_many :authors

  validates :title, :publish_year, :genre, presence: true

  enum genre: {
    'Action and Adventure': 1,
    Classics: 2,
    'Comic Book or Graphic Novel': 3,
    'Detective and Mystery': 4,
    Fantasy: 5,
    'Historical Fiction': 6,
    Horror: 7,
    Thriller: 8,
    'Literary Fiction': 9,
    Other: 0
  }
  accepts_nested_attributes_for :authors
end
