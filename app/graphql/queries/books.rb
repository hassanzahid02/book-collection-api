module Queries::Books
  class FetchBooks < Queries::BaseQuery
    type [Types::BookType], null: false

    def resolve
      Book.all.order(created_at: :desc)
    end
  end

  class FetchBook < Queries::BaseQuery
    type Types::BookType, null: false
    argument :id, ID, required: true

    def resolve(id:)
      Book.find(id)
    rescue ActiveRecord::RecordNotFound => _e
      GraphQL::ExecutionError.new('Book does not exist.')
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid attributes for #{e.record.class}:"\
                                  " #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
