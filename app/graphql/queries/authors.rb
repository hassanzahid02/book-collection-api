module Queries::Authors
  class FetchAllAuthors < Queries::BaseQuery
    type [Types::AuthorType], null: false

    def resolve
      Author.all.order(created_at: :desc)
    end
  end

  class FetchAuthor < Queries::BaseQuery
    type Types::AuthorType, null: false
    argument :id, ID, required: true

    def resolve(id:)
      Author.find(id)
    rescue ActiveRecord::RecordNotFound => _e
      GraphQL::ExecutionError.new('Author does not exist.')
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid attributes for #{e.record.class}:"\
                                  " #{e.record.errors.full_messages.join(', ')}")
    end
  end

  class FetchAuthors < Queries::BaseQuery
    type [Types::AuthorType], null: false
    argument :book_id, ID, required: true

    def resolve(book_id:)
      book_authors_ids = Book.find_by(id: book_id).authors.pluck(:id)
      Author.where.not(id: book_authors_ids)
    rescue ActiveRecord::RecordNotFound => _e
      GraphQL::ExecutionError.new('Author does not exist.')
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid attributes for #{e.record.class}:"\
                                  " #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
