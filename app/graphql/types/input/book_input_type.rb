module Types
  module Input
    class BookInputType < Types::BaseInputObject
      argument :title, String, required: true
      argument :publish_year, GraphQL::Types::ISO8601Date, required: true
      argument :genre, String, required: true
      argument :author_ids, [String], required: false

    end
  end
end
