module Types
  module Input
    class AuthorInputType < Types::BaseInputObject
      argument :first_name, String, required: true
      argument :sur_name, String, required: true
      argument :dob, GraphQL::Types::ISO8601Date, required: true
      argument :books, BookInputType, required: false
    end
  end
end
