module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :fetch_books, resolver: Queries::Books::FetchBooks
    field :fetch_book, resolver: Queries::Books::FetchBook
    field :fetch_all_authors, resolver: Queries::Authors::FetchAllAuthors
    field :fetch_authors, resolver: Queries::Authors::FetchAuthors
    field :fetch_author, resolver: Queries::Authors::FetchAuthor
  end
end
