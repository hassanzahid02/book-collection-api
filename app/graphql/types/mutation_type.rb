module Types
  class MutationType < Types::BaseObject
    field :add_book, mutation: Mutations::Books::AddBook
    field :delete_book, mutation: Mutations::Books::DeleteBook
    field :update_book, mutation: Mutations::Books::UpdateBook
    field :add_author, mutation: Mutations::Authors::AddAuthor
    field :delete_author, mutation: Mutations::Authors::DeleteAuthor
    field :update_author, mutation: Mutations::Authors::UpdateAuthor
  end
end
