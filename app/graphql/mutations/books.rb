module Mutations::Books
  class AddBook < Mutations::BaseMutation
    argument :params, Types::Input::BookInputType, required: true

    field :book, Types::BookType, null: false

    def resolve(params:)
      params = Hash params
      begin
        book = Book.create!(book_params(params))
        book.author_ids = params[:author_ids]
        { book: book }
      rescue ActiveRecord::RecordInvalid => e
        GraphQL::ExecutionError.new("Invalid attributes for #{e.record.class}:"\
                                    " #{e.record.errors.full_messages.join(', ')}")
      end
    end

    private

    def book_params(params)
      { title: params[:title], publish_year: params[:publish_year], genre: params[:genre] }
    end

    def author_params(params)
      { first_name: params[:authors][:first_name], sur_name: params[:authors][:sur_name], dob: params[:authors][:dob] }
    end
  end

  class DeleteBook < Mutations::BaseMutation
    argument :book_id, ID, required: true
    field :book, Types::BookType, null: true

    def resolve(book_id: nil)
      book = Book.find(book_id)
      book.destroy!
      { book: book }
    rescue ActiveRecord::RecordNotFound => _e
      GraphQL::ExecutionError.new('Book does not exist.')
    end
  end

  class UpdateBook < Mutations::BaseMutation
    argument :book_id, ID, required: true
    argument :title, String, required: false
    argument :publish_year, GraphQL::Types::ISO8601Date, required: false
    argument :genre, String, required: false

    field :book, Types::BookType, null: true
    field :errors, String, null: true

    def resolve(book_id:, **args)
      book = Book.find(book_id)
      book.update!(args)
      { book: book, errors: [] }
    rescue ActiveRecord::RecordNotFound => _e
      raise GraphQL::ExecutionError, 'Book does not exist.'
    rescue ActiveRecord::RecordInvalid => e
      raise GraphQL::ExecutionError, "Invalid attributes for #{e.record.class}:"\
                                     " #{e.record.errors.full_messages.join(', ')}"
    end
  end
end
