module Mutations::Authors
  class AddAuthor < Mutations::BaseMutation
    argument :params, Types::Input::AuthorInputType, required: true

    field :author, Types::AuthorType, null: false

    def resolve(params:)
      params = Hash params
      begin
        author = Author.create!(author_params(params))
        { author: author }
      rescue ActiveRecord::RecordInvalid => e
        GraphQL::ExecutionError.new("Invalid attributes for #{e.record.class}:"\
                                    " #{e.record.errors.full_messages.join(', ')}")
      end
    end

    private

    def author_params(params)
      { first_name: params[:first_name], sur_name: params[:sur_name], dob: params[:dob] }
    end
  end

  class DeleteAuthor < Mutations::BaseMutation
    argument :author_id, ID, required: true
    field :author, Types::AuthorType, null: true

    def resolve(author_id: nil)
      author = Author.find(author_id)
      author.destroy!
      { author: author }
    rescue ActiveRecord::RecordNotFound => _e
      GraphQL::ExecutionError.new('Author does not exist.')
    end
  end

  class UpdateAuthor < Mutations::BaseMutation
    argument :author_id, ID, required: true
    argument :first_name, String, required: false
    argument :dob, GraphQL::Types::ISO8601Date, required: false
    argument :sur_name, String, required: false

    field :author, Types::AuthorType, null: true
    field :errors, String, null: true

    def resolve(author_id:, **args)
      author = Author.find(author_id)
      author.update!(args)
      { author: author, errors: [] }
    rescue ActiveRecord::RecordNotFound => _e
      GraphQL::ExecutionError.new('Author does not exist.')
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid attributes for #{e.record.class}:"\
                                  " #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
