5.times do |i|
  updated_value = i + 1
  b = Book.create(title: "cal-#{updated_value}",  publish_year: Date.current - updated_value.years, genre: updated_value)
  b.authors.create(first_name: "saad", sur_name: "ullah", dob: Date.current - (updated_value * 20).years )
end