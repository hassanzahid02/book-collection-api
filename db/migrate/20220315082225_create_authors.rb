class CreateAuthors < ActiveRecord::Migration[6.1]
  def change
    create_table :authors do |t|
      t.string :first_name
      t.string :sur_name
      t.date :dob
      t.timestamps
    end
  end
end
