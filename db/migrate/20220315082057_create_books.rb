class CreateBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :books do |t|
      t.string :title
      t.date :publish_year
      t.integer :genre
      t.timestamps
    end
  end
end
